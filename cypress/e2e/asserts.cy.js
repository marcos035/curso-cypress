/// <reference types="cypress"/>



describe('Espera...', ()=>{

    beforeEach(()=>{
        cy.visit('https://wcaquino.me/cypress/componentes.html')
        cy.reload()
    
    })
        it('Equality', ()=>{

            const a = 1;

            expect(a).equal(1)
            expect(a, 'deveria ser 1').equal(1)
            expect(a).to.be.equal(1)
            expect('a').not.to.be.equal('b')
        })

        it('Truthy',()=>{
            const a= true;
            const b= false;
            let c = '';

            expect(a).to.be.true
            expect(true).to.not.be.false
            expect(true).to.be.true
            expect(b).to.not.be.true
            expect(a).to.not.be.null
            expect(c).to.be.empty
        })

        it('Object Equality',()=>{
            const obj = {
                a: 1,
                b: 2
            }    

            expect(obj).equal(obj)
            expect(obj).eql(obj)
            expect(obj).equals(obj)
            expect(obj).to.be.equal(obj)
            expect(obj).to.be.deep.equal({a:1 , b:2}) //deep para validar o objeto
            expect(obj).eql({a:1 ,  b:2})          
            expect(obj).include({a: 1})
            expect(obj).to.have.property('b')
            expect(obj).to.have.eql( { a: 1, b: 2 }) 
            expect(obj).to.not.be.empty
        })
            it("Arrays", ()=>{
                const arr = [1,2,3,4,'bola']
                expect(arr).to.have.members([1,2,3,4,'bola'])
                expect(arr).to.include.members([2,3,4],'teste')
                expect(arr).to.not.be.empty
            })

            it('Types', ()=>{
                const num =1
                const str = 'string'

                expect(num).to.be.a('number')
                expect(str).to.be.a('string')
                expect({}).to.be.an('object')
                expect([]).to.be.an('array')
            })

            it('String', ()=>{
                const str ='String de teste'

                expect(str).to.be.equal('String de teste')
                expect(str).to.have.length(15)
                expect(str).to.have.contains('de')
                expect(str).to.have.match(/de/)
                expect(str).to.have.match(/^String/)
                expect(str).to.have.match(/teste$/)
                expect(str).to.have.match(/.{15}/)
                expect(str).to.have.match(/\w+/)
                expect(str).to.have.match(/\D+/)
            })

            it("numbers",()=>{
                const number = 4;
                const floatNumber = 5.2123

                expect(number).to.be.equal(4)
                expect(number).to.be.above(3)
                expect(number).to.be.below(7)
                expect(floatNumber).to.be.equal(5.2123)
                expect(floatNumber).to.be.closeTo(5.2, 0.1)
                expect(floatNumber).to.be.above(5)
            })

})