/// <reference types="cypress"/>







describe('Espera...', ()=>{

    beforeEach(()=>{
        cy.visit('https://wcaquino.me/cypress/componentes.html')
     
    
    })

        it('Wrap', ()=>{
                const obj = { nome: 'Marcos', idade: 20}

                // expect(obj).to.have.property('nome')
                // cy.wrap(obj).should('have.property', 'nome')  

                // cy.visit('https://wcaquino.me/cypress/componentes.html')
                // cy.get('#formNome').then($el =>{

                //     cy,wrap($el).prototype('funciona via cypress')

                // })

                const promise = new promise((resolve, reject)=>{

                    setTimeout(() => {
                        resolve(10)
                    }, 500);
                })
                
                cy.get('#buttonSimple').then(()=> console.log('Encontrei o primeiro botao')) //.then Permite trabalhar com o assunto gerado no comando anterior
                // promise.then(num => console.log(num))
                cy.wrap(promise).then(ret => console.log(ret))
                cy.get('#buttonList').then(()=> console.log('Encontrei o segundo botao'))

        })

            it('its...', ()=>{      //its para pegar a propriedade de um obj/função
                const obj = {nome: 'user', idade:20}
                cy.wrap(obj).should('have.property', 'nome', 'user')
                cy.wrap(obj).its('nome').should('be.equal', 'user')


                const obj2 = {nome: 'user', idade:20, endereco:{rua: 'da saudade '}}
                cy.wrap(obj2).its('endereco').should('have.property', 'rua')
                cy.wrap(obj2).its('endereco').its('rua').should('contain', 'saudade')
                cy.wrap(obj2).its('endereco.rua').should('contain', 'saudade')

                cy.visit('https://wcaquino.me/cypress/componentes.html')
                cy.title().its('length').should('be.equal',20)

            })
                // para deixar uma entidade qualquer gerenciavel pelo cypress se usa o WRAP
            it.only('invoke...',()=>{
                // invoke: Invoque uma função no assunto gerado anteriormente.
                const getValue = () =>1;
                const soma =(a,b)=> a+b;
                cy.wrap({ fn: getValue}).invoke('fn').should('be.equal',1)
                cy.wrap({ fn: soma}).invoke('fn', 2,7).should('be.equal',9)

                cy.visit('https://wcaquino.me/cypress/componentes.html')
                cy.get('#formNome').invoke('val', 'texto via invoke')

                cy.window().invoke('alert','alouuuuuuuuuuuu')

                cy.get('#resultado').invoke('html', '<input type="button" value="hacked"/>')


            })



})