/// <reference types="cypress"/>




describe('fixtures testes' ,()=>{
  
    it('Get data form fixture file',function(){
        cy.visit('https://wcaquino.me/cypress/componentes.html')
                cy.fixture('userData').as('usuario').then(()=>{

                cy.get('#formNome').type(this.usuario.nome)
                cy.get('#formSobrenome').type(this.usuario.sobrenome)
                cy.get(`#formSexo [value=${this.usuario.sexo}]`).click()
                cy.get(`#formComidaFavorita [value=${this.usuario.comida}]`).click()
                cy.get('#formEscolaridade').select(this.usuario.escolaridade)
                cy.get('#formEsportes').select(this.usuario.esportes)

                cy.get('#formCadastrar').click()
                cy.get('#resultado > :nth-child(1)').should('contain','Cadastrado!')
            })
               

    })

})



// const social = ['Face', ' youtube', 'twitch']
// social.forEach((socials)=>{

//     console.log(socials)

// })