/// <reference types="cypress"/>

import loc from '../support/pageObjects'
import '../support/commandsContas'

describe('Automation Seu Barriga', ()=>{

    
    before(()=>{
        cy.intercept('POST', '/signin', (req) => {
            req.reply({
                statusCode: 200,
                body: {
                    id: 1000,
                    nome: 'Usuario fake',
                    token: 'Uma string para simular token fake'
                }

                
            })
        }).as('signin')

        cy.intercept('POST', '/extrato', (req) => {
            req.reply({
                
                statusCode: 201,
                body: {
                    conta_id:  1790647,
                    descricao: 'Conta com login fake',
                    envolvido: 'envolvido no crimw',
                    nome: 'Usuario fake',
                    status: true,
                    valor: '1000000'
                    
                },
              
            })
        })






         
        cy.login('marcos@gmail.com','senha errada')
    })
    beforeEach(()=>{
       
        cy.resetAPP()
        // cy.get(loc.LOGIN.USER).type('marcos123@gmail.com')
        // cy.get(loc.LOGIN.PASSWORD).type('12345')
        // cy.get(loc.LOGIN.BTN_LOGIN).click()
        // cy.get(loc.MESSAGE).should('exist')
       
    })

    it.only('Inserir Conta',()=>{
           cy.acessarMenuConta()
        // cy.get(loc.MENU.SETTINGS).click()
        // cy.get(loc.MENU.ACCOUNT).click()

            cy.InserirConta('Conta alterada')
        // cy.get(loc.CONTAS.NOME).type('Conta de Teste22')
        // cy.get(loc.CONTAS.BTN_SALVAR).click()
        // cy.get(loc.MESSAGE).should('contain', 'Conta inserida com sucesso')
    })

    it('Alterar conta', ()=>{

        cy.get(loc.MENU.SETTINGS).click()
        cy.get(loc.MENU.ACCOUNT).click()

        cy.get('tbody > :nth-child(2) > :nth-child(2) > :nth-child(1) > .far').click()
        cy.get(loc.CONTAS.NOME)
        .clear()
        .type('Conta alterada')
        cy.get(loc.CONTAS.BTN_SALVAR).click()
        cy.get(loc.MESSAGE).should('contain', '!')
        
        
    })

    it('should not create an account with same name',()=>{

        cy.acessarMenuConta()
        cy.get(loc.CONTAS.NOME).type('Conta para saldo')
        cy.get(loc.CONTAS.BTN_SALVAR).click()
        cy.get(loc.MESSAGE).should('contain', 'code 400')


    })
    it('Movimentaçoes', ()=>{
        cy.get(loc.MENU.MOVIMENT).click()
        cy.get(loc.MOVIMENTACAO.DESCRICAO).type('Conta para alterar')
        cy.get(loc.MOVIMENTACAO.SELECT_CONTA).select('Conta para movimentacoes')
        cy.get(loc.MOVIMENTACAO.ENVOLVIDO).type('Marcos Fideles')
        cy.get(loc.MOVIMENTACAO.VALOR).type('3500')
        cy.get(loc.MOVIMENTACAO.BTN_SUCESS).click()
        cy.get(loc.MOVIMENTACAO.VALIDAR).click()
        cy.get(loc.MOVIMENTACAO.SALVAR).click()
        cy.get(loc.MESSAGE).should('contain','Movimentação inserida com sucesso')
        cy.get('.list-group >li').should('have.length', 7)

    })

    it('Validar Valor',()=>{
        cy.get(loc.MENU.HOME).click().then(()=>{

            setTimeout(1000)
        })
        
        cy.get(loc.SALDO.FN_SALDO).should('contain','534')

    })


})


// marcos123@gmail.com
// 12345







