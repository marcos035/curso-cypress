/// <reference types="cypress"/>





describe('Espera...', ()=>{

    beforeEach(()=>{
        cy.visit('https://wcaquino.me/cypress/componentes.html')
        cy.reload()
    
    })

    it('Deve aguardar o elemento estar disponivel',()=>{
         cy.get('#buttonDelay')
         .click()
         cy.get('#novoCampo')
         .type('Funciona')
         .should('have.value','Funciona')
         

    })

        it('Uso do Find',()=>{
            cy.get('#buttonList')
            .click()
            cy.get('#lista li')
                .find('span') //Obtenha os elementos DOM descendentes de um seletor específico.
                .should('contain', 'Item 1')   

                cy.get('#lista li span')
               
                .should('contain', 'Item 2')   
        }) 

        it('Uso do timeout', ()=>{
                // cy.get('#buttonDelay')
                //  .click()
                // cy.get('#novoCampo',{Timeout: 1000})
                //  .should('exist')
                cy.get('#buttonList')
                .click()
                cy.get('#lista li span', {timeout:7000})
                .should('have.length',2)   

        })
            // cy.wait(5000) colocar apenas em casos especificos- vai deixar a aplicação lenta 
                it.only('Click retry', ()=>{
                    cy.get('#buttonCount')
                    .click()
                    .click()
                    .should('have.value','111')

                    var titulo;
                    

                    cy.title().then(title =>{  //.then Permite trabalhar com o assunto gerado no comando anterior
                        titulo = title
                        console.log(titulo)
                        
                        cy.get('#elementosForm\\:sugestoes')
                        .type(`${titulo} Marcos Fideles, testes cypress manipulação de DOM`)
                    })
                    cy.get('#tabelaUsuarios > :nth-child(2) > :nth-child(1) > :nth-child(6) > input').then(teste=>{

                         cy.wrap(teste).type(titulo) 

                        
                    })

                    // cy.get(':nth-child(2) > :nth-child(6) > input').then(teste3=>{
                    //     cy.wrap(teste3).type('Marcos').its('length').should('be.equal', 'Marcos')
                    // })
                }) 

                
})