/// <reference types="cypress"/>




describe('fixtures testes' ,()=>{
   

    beforeEach(()=>{
        cy.visit('https://barrigareact.wcaquino.me/login')
        
    })
    const comidas = [ 'carne','frango','pizza','vegetariano']
    it(`Cadastro com comida ${comidas}`,()=>{
                
               comidas.forEach(comida=>{
                cy.reload()
                cy.get('#formNome').type("Marcos")
                cy.get('#formSobrenome').type("Fideles")
                cy.get(`#formSexo [value=M]`).click()
                cy.get(`#formComidaFavorita [value= ${comida}`).click()
                cy.get('#formEscolaridade').select("Mestrado")
                cy.get('#formEsportes').select("Corrida")

                cy.get('#formCadastrar').click()
                cy.get('#resultado > :nth-child(1)').should('contain','Cadastrado!')
                
            })
    })


    it.only('Deve selecionar todos usando o EACH', ()=>{

            cy.get('#formNome').type("Marcos")
            cy.get('#formSobrenome').type("Fideles")
            cy.get(`#formSexo [value=M]`).click()

            cy.get(`[name=formComidaFavorita]`).each($el=>{
                if($el.val() != 'vegetariano')
                cy.wrap($el).click()
            })

            cy.get('#formEscolaridade').select("Mestrado")
            cy.get('#formEsportes').select("Corrida")

            cy.get('#formCadastrar').click()
            cy.get('#resultado > :nth-child(1)').should('contain','Cadastrado!')
    
    })
    

})


// aula testes dinamicos muito boa, rever