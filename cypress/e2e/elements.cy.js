/// <reference types= "cypress" />

describe("work with basic elements", ()=>{
    
        beforeEach(()=>{
            cy.visit('https://wcaquino.me/cypress/componentes.html')
            cy.reload()
        
        })

        it('Text', ()=>{
            
            cy.get('.facilAchar').should('contain', 'Cuidado')
            cy.get('span').should('contain', 'Cuidado')
            cy.get('.facilAchar').should('have.text', 'Cuidado onde clica, muitas armadilhas...')

        })


        it('Links', ()=>{
            

            cy.get('[href="#"]').click()
            cy.get('#resultado').should('have.text', 'Voltou!')

            cy.reload()
            cy.get('#resultado').should('have.not.text', 'Voltou')
            cy.contains('Voltar').click()
            cy.get('#resultado').should('have.text', 'Voltou!')


        })

        it('TextFields', ()=>{

            cy.get('#formNome').type('cypress Teste')
            cy.get('#formNome').should('have.value', 'cypress Teste')

            cy.get('#elementosForm\\:sugestoes')
            .type('textarea')
            .should('have.value', 'textarea')

            cy.get('#tabelaUsuarios > :nth-child(2) > :nth-child(1) > :nth-child(6) > input')
            .type('Marcola del snipa')
            .should('have.value', 'Marcola del snipa')

            cy.get('[data-cy="dataSobrenome"]')
            .type('senha123456{backspace}{backspace}')

            cy.get('#elementosForm\\:sugestoes')
            .clear()
            .type('Erro{selectall}acerto',{delay:100})
            .should('have.value', 'acerto')
        })

        it('Radio Button', ()=>{
            cy.get('#formSexoFem')
            .click()
            .should('be.checked')

            cy.get('#formSexoMasc').should('not.be.checked')

            cy.get('[name="formSexo"]').should('have.length', 2)

        })

        it('CheckBox', ()=>{
            cy.get('#formComidaPizza')
            .click()
            .should('be.checked')
            
            cy.get('[name=formComidaFavorita]').click({multiple:true})
            cy.get('#formComidaPizza').should('not.be.checked')
            cy.get('#formComidaFrango').should('be.checked')


        })

    
            it.only('Combo', ()=>{
                cy.get('[data-test="dataEscolaridade"]')
                .select('Mestrado')
                .should('have.value','mestrado' )

                cy.get('[data-test="dataEscolaridade"]')
                .select('1o grau completo')
                .should('have.value','1graucomp' )

                cy.get('[data-test="dataEscolaridade"] option').should('have.length', 8)
                cy.get('[data-test="dataEscolaridade"] option').then($arr=>{

                    const values = [];
                    $arr.each(function(){
                        values.push(this.innerHTML)

                    })
                    expect(values).to.include.members(["Superior", 'Mestrado'])
                })


            })

            it.only('Combo Multiplo', ()=>{
                cy.get('[data-testid="dataEsportes"]')
                .select(['futebol','Corrida'])
                // .should('have.value',['Corrida','futebol'])
                cy.get('[data-testid="dataEsportes"]').then($el=>{
                    expect($el.val()).to.be.deep.equal(['futebol','Corrida'])
                    expect($el.val()).to.have.length(2)
                })

                cy.get('[data-testid="dataEsportes"]')
                .invoke('val')
                .should('eql',['futebol','Corrida'])
                

            })
            
            



            
        
})