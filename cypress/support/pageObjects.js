const locators ={

  LOGIN: {
    USER:'.input-group > .form-control',
    PASSWORD:':nth-child(2) > .form-control',
    BTN_LOGIN: '.btn'
  },
  MENU:{
    SETTINGS:'.dropdown-toggle',
    ACCOUNT:'[href="/contas"]',
    BTN_SALVAR:'.btn',
    RESET:'[href="/reset"]',
    MOVIMENT: ':nth-child(2) > .nav-link > .fas',
    HOME: ':nth-child(1) > .nav-link'

  },
  CONTAS:{
    NOME:'.form-control',
    BTN_SALVAR:'.btn',
    ALTERAR_ACCOUNT: ''
  },

  MOVIMENTACAO:{
    BTN_SUCESS: '.btn-success',
    BTN_NEGATIVE: '.btn-secondary',
    DESCRICAO:  '#descricao',
    ENVOLVIDO:  '#envolvido',
    SELECT_CONTA: ':nth-child(3) > :nth-child(2) > .form-control',
    VALOR:  '.col-4 > .form-control',
    VALIDAR:  '.col-2 > .btn',
    SALVAR: '.btn-primary'
  },
  MESSAGE: '.toast-message',

  SALDO: {
    FN_SALDO: ':nth-child(3) > :nth-child(2)'
  },

}

  export default locators;
 
  