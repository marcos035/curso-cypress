
import loc from '../support/pageObjects'

Cypress.Commands.add('acessarMenuConta',()=>{
    cy.get(loc.MENU.SETTINGS).click()
    cy.get(loc.MENU.ACCOUNT).click()

})

Cypress.Commands.add('InserirConta',(conta)=>{

    cy.get(loc.CONTAS.NOME).type(conta)
    cy.get(loc.CONTAS.BTN_SALVAR).click()

})
